from __future__ import unicode_literals
import os
from glob import glob
import json
import pandas 
import datetime
import sys
import logging
class ParseData:
  def __init__(self):
        self.today = datetime.datetime.today().strftime('%Y-%m-%d')
  def ProcessCSV(self,n,filename,hospitalId, charge, description,category,destination):
      if filename.endswith('csv'):
         print("Parsing %s" % filename)
      content = pandas.read_csv(filename,skiprows=n,encoding= 'unicode_escape')
      for col in content.columns:
          if description in col.lower():
              description = col
          if charge in col.lower():
              charge = col
      content = pandas.read_csv(filename, skiprows=n, usecols=[charge, description], encoding='unicode_escape')

      content.rename(columns={charge: 'Charge', description: 'Description'}, inplace=True)
      # content.insert (1, 'hospitalId' ,hospitalId)
      content['Category'] = category
      content['Charge']=content['Charge'].astype(str).str.replace(',','')
      content['Charge']=content['Charge'].astype(str).str.replace('$','')
      content['Charge'] = content['Charge'].astype(str).str.replace(' ', '')
      content = content.dropna(how='all')
      # Save data!
      print(content.shape)
      content = content[['Description', 'Charge', 'Category']]
      if os.path.exists(destination):
          print('Saving')
          content.to_csv(destination, mode='a',header=False, encoding='utf-8', index=False)
      else:

          content.to_csv(destination, mode='a',encoding='utf-8', index=False)
  def ProcessXLSX(self,n,filename,hospitalId, charge, description,category,destination,sheet=None):
   
      if filename.endswith('xlsx'):
        print("Parsing %s" % filename)
      if sheet==None:
       content = pandas.read_excel(filename,skiprows=n,encoding= 'unicode_escape')
      else:
       content = pandas.read_excel(filename, sheet, skiprows=n,  encoding='unicode_escape')
      for col in content.columns:
          if col.isalpha() and description.lower() in col.lower():
              description = col
          if col.isalpha() and charge.lower() in col.lower():
              charge = col
      if sheet == None:
          content = pandas.read_excel(filename, skiprows=n, usecols=[charge, description], encoding='unicode_escape')
      else:
          content = pandas.read_excel(filename, sheet, skiprows=n, usecols=[charge, description], encoding='unicode_escape')

      content.rename(columns={charge:'Charge',description:'Description'},inplace=True)

      #content.insert (1, 'hospitalId' ,hospitalId)
      content['Category'] = category
      content['Charge'] = content['Charge'].astype(str).str.replace(',', '')
      content['Charge'] = content['Charge'].astype(str).str.replace('$', '')
      content['Charge'] = content['Charge'].astype(str).str.replace(' ', '')
      content = content.dropna(how='all')
      # Save data!
      print(content.shape)
      content = content[['Description', 'Charge', 'Category']]
      if os.path.exists(destination):
         content.to_csv(destination, mode='a',header=False, encoding='utf-8', index=False)
      else:
         content.to_csv(destination, mode='a',encoding='utf-8', index=False)