# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import os
from urllib.parse import urljoin
from urllib.parse import urlparse
import json
import urllib.request
import requests


class ScrapcdmPipeline:
    def process_item(self, item, spider):
         url = item['url']
         path= item['path']
         r = requests.get(url,headers={'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.100 Safari/537.36'})
         with open(path, 'wb') as f:
              f.write(r.content)
         outdir = os.path.join("Data")
         directory = item['directory']
         state=item['state']
         path = os.path.join(outdir, state)
         path = os.path.join(path, directory)
         records_file = os.path.join(path, 'records.json')
         records=item['records']
         with open(records_file, 'w') as filey:
            filey.write(json.dumps(records, indent=4))
         return item

