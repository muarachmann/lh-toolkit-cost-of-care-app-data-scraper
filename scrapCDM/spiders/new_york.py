import scrapy
import os
from scrapCDM.items import ScrapcdmItem
from bs4 import BeautifulSoup

urls = [
    ["https://health.data.ny.gov/Health/Hospital-Inpatient-Cost-Transparency-Beginning-200/7dtz-qxmr", "", "", "New York"]
]
class NewYorkSpider(scrapy.Spider):
    name = 'new_york'

    def start_requests(self):
        for url in urls:
            yield scrapy.Request(url[0], callback=self.parse,
                                  meta={'index': url[1], 'name': url[2], 'state': url[3]})

    def parse(self, response):
        records = []
        soup = BeautifulSoup(response.text, 'lxml')
        for entry in soup.find_all('a', href=True):
            link = entry['href']
            entry_name = entry.text.strip()
            self.logger.info(link)
            if '.csv' in link and 'plot' in link:
                link = link.split('=')[1]
                base_url = response.meta['index']
                link = base_url + link
                self.logger.info(link)
                yield from self.save(link, entry_name, response.meta['name'], response.meta['state'], records)
                # Keep json record of all files included

    def save(self, url, hospitalId, name, state, records):
        filename = os.path.basename(url.split('?')[0])
        self.logger.info('filename')
        self.logger.info(url)
        outdir = os.path.join("Data")
        if os.path.isdir(outdir) == False:
            os.mkdir(outdir)
        path = os.path.join(outdir, state)
        if os.path.isdir(path) == False:
            os.mkdir(path)
        directory = name
        path = os.path.join(path, directory)
        if os.path.isdir(path) == False:
            os.mkdir(path)
        path = os.path.join(path, filename)
        self.log("\n\n\n We got data! \n\n\n" + path)
        self.logger.info('Saving File %s', path)
        records = records
        record = {
            'filename': filename,
            'hospitalId': hospitalId,  # sub hospital name
            'name': name  # hospital name in overpass api
        }
        records.append(record)
        item = ScrapcdmItem()
        item['url'] = url
        item['path'] = path
        item['state'] = state
        item['directory'] = directory
        item['records'] = records
        yield item
