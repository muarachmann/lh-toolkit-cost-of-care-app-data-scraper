import pandas
import pandas as pd
import sys, subprocess, os
from io import StringIO
import pyodbc

VERBOSE = True
def mdb_to_pandas_windows(database_path):
    connStr = (
        r"DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};"
        r"DBQ="+database_path+";"
    )
    connection = pyodbc.connect(connStr)
    cursor=connection.cursor()
    out_tables = {}
    tab=""
    for table in cursor.tables():
        tab=table.table_name
    out_tables[tab] = pandas.read_sql("Select * from {0}".format(tab),connection)
    return out_tables

def mdb_to_pandas(database_path):
    subprocess.call(["mdb-schema", database_path, "mysql"])
    # Get the list of table names with "mdb-tables"
    table_names = subprocess.Popen(["mdb-tables", "-1", database_path],
                                   stdout=subprocess.PIPE).communicate()[0]
    tables = table_names.splitlines()
    sys.stdout.flush()
    # Dump each table as a stringio using "mdb-export",
    out_tables = {}
    for rtable in tables:
        table = rtable.decode()

        if VERBOSE: print('running table:', table)
        if table != '':
            if VERBOSE: print("Dumping " + table)
            contents = subprocess.Popen(["mdb-export", database_path, table],
                                        stdout=subprocess.PIPE).communicate()[0]
            temp_io = StringIO(contents.decode())
            print(table, temp_io)
            out_tables[table] = pd.read_csv(temp_io)
    return out_tables


def show_data(path, table):
    print(path)
    tables = subprocess.check_output(["mdb-export", path, table])
    return tables.decode().split('\n')


def convert_df(path, table):
    d = show_data(path, table)
    columns = d[0].split(',')
    data = [i.split(',') for i in d[1:]]
    df = pd.DataFrame(columns=columns, data=data)
    return df


# Getting current Location of file
here = os.path.dirname(os.path.abspath(__file__))

basePath = here.split("Process CDM")[0]
newd = os.path.join(basePath, "CDM")  # Desctination to store CDM
if os.path.isdir(newd) == False:
    os.mkdir(newd)
path = os.path.join(os.path.join(basePath, 'Data'), "Indiana")
if not os.path.exists(path):
    print("No Data Found For {0}".format(path))
    exit(0)
here = path

for subdir, dirs, files in os.walk(here):
    # Reading ID of Each file with their corresponding files
    if 'HOSPITAL ID' in subdir:
        for file in os.listdir(subdir):
            print(file)
            path = os.path.join(subdir, file)
            hospitalId = pandas.read_excel(path, usecols=['Hospital_ID', 'Hospital_Name'])
            print(hospitalId[hospitalId['Hospital_ID'] == 3]['Hospital_Name'])

    if 'MS-DRG ID' in subdir:
        for file in os.listdir(subdir):
            print(file)
            path = os.path.join(subdir, file)
            msdrgId = pandas.read_excel(path)
            print(msdrgId[msdrgId['MSDRG'] == 3]['MSDRG_DESCRIPTION'])
    elif 'MS-DRG' in subdir:
        for file in os.listdir(subdir):
            if 'mdb' in file.lower():
                path = os.path.join(subdir, file)
                print(path)
                print('PRINTING NOW')
                out_tables = mdb_to_pandas(path)
                msdrg = out_tables["MSDRG"]
                msdrg = msdrg[["HOSPITAL_ID", "MSDRG", "XC"]]

    if 'DIAGNOSIS ID' in subdir:
        for file in os.listdir(subdir):
            print(file)
            path = os.path.join(subdir, file)
            diagId = pandas.read_excel(path)
            print(diagId[diagId['ICD_Diagnosis_Code'] == 3]['Diagnosis_Description'])
    elif 'DIAGNOSIS' in subdir:
        for file in os.listdir(subdir):
            if 'mdb' in file.lower():
                path = os.path.join(subdir, file)
                out_tables = mdb_to_pandas(path)
                diagnosis = out_tables["diagnosis"]
                diagnosis = diagnosis[["HOSPITAL_ID", "DIAGNOSIS_1", "XC"]]
                print(diagnosis.head())

    if 'APR-DRG ID' in subdir:
        for file in os.listdir(subdir):
            print(file)
            path = os.path.join(subdir, file)
            aprId = pandas.read_excel(path)
            print(aprId.head)
            print(aprId[aprId['APRDRG'] == 3]['APRDRG_DESCRIPTION'])
    elif 'APR-DRG' in subdir:
        for file in os.listdir(subdir):
            if 'mdb' in file.lower():
                path = os.path.join(subdir, file)
                print('WORK')
                print(path)
                print('PRINTING NOW')
                out_tables = mdb_to_pandas(path)
                aprdrg = out_tables["APRDRG"]
                print(aprdrg.head())
                aprdrg = aprdrg[["HOSPITAL_ID", "APRDRG", "XC"]]

# Mapping Id with name for Diagnosis

id = {}
id = dict(zip(hospitalId['Hospital_ID'], hospitalId['Hospital_Name']))
diagnosis['HOSPITAL_ID'] = diagnosis['HOSPITAL_ID'].map(id)
id = dict(zip(diagId['ICD_Diagnosis_Code'], diagId['Diagnosis_Description']))
diagnosis['DIAGNOSIS_1'] = diagnosis['DIAGNOSIS_1'].map(id)

diagnosis.rename(columns={'HOSPITAL_ID': 'Name', 'DIAGNOSIS_1': 'Description', 'XC': 'Charge'}, inplace=True)
diagnosis['Category'] = 'Standard'
print(diagnosis.head)

destination = os.path.join(newd, "Indiana")  # Creating Indiana Directory
if os.path.isdir(destination) == False:
    os.mkdir(destination)

# Processing Diagnosis
i = 0
prevV = ""
prevI = 0
nrow = diagnosis.shape[0] - 1
for index, row in diagnosis.iterrows():
    if prevV != row["Name"]:
        if (prevI != 0):
            print(index)
            prevV = prevV.replace('/', '')
            destination = os.path.join(newd, "Indiana")
            check = os.path.join(destination, prevV + ".csv")
            if os.path.exists(check):
                os.remove(check)
            destination = os.path.join(destination, prevV + ".csv")
            df = diagnosis[prevI:index]
            df = df.rename(columns={"XC": "Charge"})
            df.to_csv(destination, mode='a', encoding='utf-8', index=False,
                      columns=['Description', 'Charge', 'Category'])
        prevV = row["Name"]
        prevI = index
    if index == nrow:
        print(prevV)
        print(index)
        destination = os.path.join(newd, "Indiana")
        check = os.path.join(destination, prevV + ".csv")
        if os.path.exists(check):
            os.remove(check)
        destination = os.path.join(destination, prevV + ".csv")
        df = diagnosis[prevI:index]
        df = df.rename(columns={"XC": "Charge"})
        df.to_csv(destination, mode='a', encoding='utf-8', index=False, columns=['Description', 'Charge', 'Category'])

# Mapping Id with name for MS DRG
#######################################

id = {}
id = dict(zip(hospitalId['Hospital_ID'], hospitalId['Hospital_Name']))
msdrg['HOSPITAL_ID'] = msdrg['HOSPITAL_ID'].map(id)
id = dict(zip(msdrgId['MSDRG'], msdrgId['MSDRG_DESCRIPTION']))
msdrg['MSDRG'] = msdrg['MSDRG'].map(id)

msdrg.rename(columns={'HOSPITAL_ID': 'Name', 'MSDRG': 'Description', 'XC': 'Charge'}, inplace=True)
msdrg['Category'] = 'DRG'
print(msdrg.head)

# Processing MS DRG
i = 0
prevV = ""
prevI = 0
nrow = msdrg.shape[0] - 1
for index, row in msdrg.iterrows():
    if prevV != row["Name"]:
        if (prevI != 0):
            print(index)
            prevV = prevV.replace('/', '')

            destination = os.path.join(newd, "Indiana")
            destination = os.path.join(destination, prevV + ".csv")
            df = msdrg[prevI:index]
            df = df.rename(columns={"XC": "Charge"})
            df.to_csv(destination, mode='a', encoding='utf-8', index=False, header=None,
                      columns=['Description', 'Charge', 'Category'])
        prevV = row["Name"]
        prevI = index
    if index == nrow:
        print(prevV)
        print(index)
        destination = os.path.join(newd, "Indiana")
        destination = os.path.join(destination, prevV + ".csv")
        df = msdrg[prevI:index]
        df = df.rename(columns={"XC": "Charge"})
        df.to_csv(destination, mode='a', encoding='utf-8', index=False, header=None,
                  columns=['Description', 'Charge', 'Category'])

# Mapping Id with name
id = {}
id = dict(zip(hospitalId['Hospital_ID'], hospitalId['Hospital_Name']))
aprdrg['HOSPITAL_ID'] = aprdrg['HOSPITAL_ID'].map(id)
id = dict(zip(aprId['APRDRG'], aprId['APRDRG_DESCRIPTION']))
aprdrg['APRDRG'] = aprdrg['APRDRG'].map(id)
# Renaming to proper column names
aprdrg.rename(columns={'HOSPITAL_ID': 'Name', 'APRDRG': 'Description', 'XC': 'Charge'}, inplace=True)
aprdrg['Category'] = 'DRG'
print(aprdrg.head)

# Processing APR DRG
i = 0
prevV = ""
prevI = 0
nrow = aprdrg.shape[0] - 1
for index, row in aprdrg.iterrows():
    if prevV != row["Name"]:
        if (prevI != 0):
            print(index)
            prevV = prevV.replace('/', '')
            destination = os.path.join(newd, "Indiana")
            destination = os.path.join(destination, prevV + ".csv")
            df = aprdrg[prevI:index]
            df = df.rename(columns={"XC": "Charge"})
            df.to_csv(destination, mode='a', encoding='utf-8', index=False, header=None,
                      columns=['Description', 'Charge', 'Category'])
        prevV = row["Name"]
        prevI = index
    if index == nrow:
        print(prevV)
        print(index)

        destination = os.path.join(newd, "Indiana")
        destination = os.path.join(destination, prevV + ".csv")
        df = aprdrg[prevI:index]
        df = df.rename(columns={"XC": "Charge"})

        df.to_csv(destination, mode='a', encoding='utf-8', index=False, header=None,
                  columns=['Description', 'Charge', 'Category'])
