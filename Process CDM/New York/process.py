import os

import pandas
cnt=0
here = os.path.dirname(os.path.abspath(__file__))
folder = os.path.basename(here)

basePath=here.split("Process CDM")[0]
path = os.path.join(os.path.join(basePath, 'Data'),"New York")
if not os.path.exists(path):
    print("No Data Found For {0}".format(path))
    exit(0)
newd=os.path.join(basePath,"CDM")
if os.path.isdir(newd)==False:
     os.mkdir(newd)

destination = os.path.join(newd, "New York")
if os.path.isdir(destination)==False:
     os.mkdir(destination)
destination = os.path.join(destination, 'All Hospitals' + ".csv")
if os.path.exists(destination) and cnt !=1:
     os.remove(destination)
     cnt=1
path = os.path.join(path, 'rows.csv')
content = pandas.read_csv(path,usecols=['Facility Name', 'APR DRG Description','APR Medical Surgical Description','Mean Charge'],encoding= 'unicode_escape')
content.sort_values(by=['Facility Name'], inplace=True)
content.rename(columns={'Facility Name':'Name','Mean Charge': 'Charge','APR Medical Surgical Description':'Category', 'APR DRG Description': 'Description'}, inplace=True)
content = content.dropna(how='all')
# Save data!
print(content.shape)

i=0
prevV=""
prevI=0
nrow=content.shape[0] -1
for index, row  in content.iterrows():
       if prevV!=row["Name"]:
            if(prevI!=0):
                 print(index)
                 prevV=prevV.replace('/', '')
                 destination = os.path.join(newd, "New York")
                 check = os.path.join(destination, prevV + ".csv")
                 if os.path.exists(check):
                      os.remove(check)
                 destination = os.path.join(destination, prevV + ".csv")
                 df=content[prevI:index]
                 df.to_csv(destination, mode='a', encoding='utf-8', index=False,columns=['Description', 'Charge','Category'])
            prevV=row["Name"]
            prevI=index
       if index == nrow:
            print(prevV)
            print(index)
            destination = os.path.join(newd, "New York")
            check = os.path.join(destination, prevV + ".csv")
            if os.path.exists(check):
                 os.remove(check)
            destination = os.path.join(destination, prevV + ".csv")
            df = content[prevI:index]
            df.to_csv(destination, mode='a', encoding='utf-8', index=False,columns=['Description', 'Charge','Category'])
