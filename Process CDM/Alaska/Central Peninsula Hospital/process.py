import importlib
import os
import sys
import json

sys.path.append(os.getcwd())

from ParseData import ParseData
a = ParseData()
here = os.path.dirname(os.path.abspath(__file__))
folder = os.path.basename(here)

basePath=here.split("Process CDM")[0]
destination=os.path.join(basePath,"CDM")
pathToData=os.path.join(basePath,'Data')
here=os.path.join(os.path.join(pathToData,'Alaska'),folder)
if not os.path.exists(here):
    print("No Data Found For {0}".format(here))
    exit(0)

if not os.path.exists(destination):
    os.mkdir(destination)
print(destination)
state = os.path.join(destination, 'Alaska')  # Storing Statewise
if not os.path.exists(state):
    os.mkdir(state)
destination = os.path.join(state, folder+".csv")
if os.path.exists(destination):
    os.remove(destination)     #Removing from CDM
results_json = os.path.join(here, 'records.json')
with open(results_json, 'r') as filey:
    results = json.loads(filey.read())
for result in results:

    filename = os.path.join(here, result['filename'])
    hospitalId=result['hospitalId']
    name=result['name']
    if not os.path.exists(filename):
        print('%s is not found in latest folder.' % filename)
        continue

    if os.stat(filename).st_size == 0:
        print('%s is empty, skipping.' % filename)
        continue
    if 'average' in filename.lower():
        n=4
        category = 'DRG'
        description ='DRG_NAME'
        charge='AVG_HSP_ACCOUNT_TOT_CHGS'
        a.ProcessCSV(n,filename,hospitalId,charge,description,category,destination)
    elif 'supplies' in filename.lower():
       category='Pharmacy'
       n=7
       description = 'CHARGE DESCRIPTION'
       charge = 'unit'
       a.ProcessCSV(n,filename,hospitalId,charge,description,category,destination)