import importlib
import os
import sys
import json
import pandas as pd


def ProcessXLSX( n, path, filename, hospitalId, charge, description, category, destination, sheet=None):
    if filename.endswith('xlsx'):
        print("Parsing %s" % filename)
    if sheet == None:
        content =pd.read_excel(path, skiprows=n, usecols=[charge, description], encoding='unicode_escape')
    else:
        content = pd.read_excel(path, sheet, skiprows=n, usecols=[charge, description], encoding='unicode_escape')
    content.rename(columns={charge: 'Charge', description: 'Description'}, inplace=True)
    content.insert (1, 'hospitalId' ,hospitalId)
    content['Category'] = category
    content = content.dropna(how='all')
    # Save Data!
    print(content.shape)
    if os.path.exists(destination):
        content.to_csv(destination, mode='a', header=False, encoding='utf-8', index=False)
    else:
        content.to_csv(destination, mode='a', encoding='utf-8', index=False)

cnt=0
here = os.path.dirname(os.path.abspath(__file__))
folder = os.path.basename(here)

basePath=here.split("Process CDM")[0]
destination=os.path.join(basePath,"CDM")
pathToData=os.path.join(basePath,'Data')
here=os.path.join(os.path.join(pathToData,'Alaska'),folder)
if not os.path.exists(here):
    print("No Data Found For {0}".format(here))
    exit(0)
results_json = os.path.join(here, 'records.json')
if not os.path.exists(results_json):
    print('%s does not have results.json' % folder)
    sys.exit(1)
with open(results_json, 'r') as filey:
    results = json.loads(filey.read())
for result in results:
    destination = os.path.join(basePath, "CDM")
    state = os.path.join(destination, 'Alaska')
    destination = os.path.join(state, result['hospitalId']+".csv")
    if os.path.exists(destination) and cnt !=1:
        os.remove(destination)
        cnt=1
    filename = os.path.join(here, result['filename'])
    hospitalId=result['hospitalId']
    name=result['name']
    if not os.path.exists(filename):
        print('%s is not found in latest folder.' % filename)
        continue

    if os.stat(filename).st_size == 0:
        print('%s is empty, skipping.' % filename)
        continue
    charge=""
    description=""
    if 'DRG' in filename.lower():
        n=3
        category = 'DRG'
        df=pd.read_csv(filename,skiprows=n)
        for col in df.columns:
            if 'Description' in col:
                description = col

            if 'Average Charges' in col:
             charge=col

    else:
       category='Standard'
       n=3
       print(destination)
       xls = pd.ExcelFile(filename)
       for sheet in xls.sheet_names:
        if filename.endswith('csv'):
         df = pd.read_csv(filename,sheet, skiprows=n)
        else:
         df = pd.read_excel(filename, sheet,skiprows=n)
        if 'Pricing' in sheet:
         for col in df.columns:
           if 'Description' in col:
               description = col
           if 'Price' in col:
               charge = col

        else:
            category="DRG"
            for col in df.columns:
                if 'Description' in col:
                    description = col
                if 'Average Charges' in col:
                    charge = col
        if len(charge)!=0 and len(description)!=0:
         # Facility', 'DRG', 'DRG Description', 'Average Covered Charges
         if filename.endswith('csv'):
            a = ParseData()
            a.ProcessCSV(n,filename,result['filename'],hospitalId,charge,description,category,destination)
         elif filename.endswith('xlsx'):
            ProcessXLSX(n,filename,result['filename'],hospitalId,charge,description,category,destination,sheet)