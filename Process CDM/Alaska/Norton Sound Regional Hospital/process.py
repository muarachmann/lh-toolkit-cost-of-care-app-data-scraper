import datetime
import importlib
import os
import sys
import json
import pandas

sys.path.append(os.getcwd())

from ParseData import ParseData

a = ParseData()
here = os.path.dirname(os.path.abspath(__file__))
folder = os.path.basename(here)

basePath=here.split("Process CDM")[0]
destination=os.path.join(basePath,"CDM")
pathToData=os.path.join(basePath,'Data')
here=os.path.join(os.path.join(pathToData,'Alaska'),folder)
if not os.path.exists(here):
    print("No Data Found For {0}".format(here))
    exit(0)
if not os.path.exists(here):
    print("No Data Found")
    exit(0)

if not os.path.exists(destination):
    os.mkdir(destination)
print(destination)
state = os.path.join(destination, 'Alaska')  # Storing Statewise
if not os.path.exists(state):
    os.mkdir(state)
destination = os.path.join(state, folder + ".csv")

for filename in os.listdir(here):
    name = folder
    filename = os.path.join(here, filename)

    if not os.path.exists(filename):
        print('%s is not found in latest folder.' % filename)
        continue

    if os.stat(filename).st_size == 0:
        print('%s is empty, skipping.' % filename)
        continue
    n = 0
    category = "Standard"
    description = 'Description'
    charge = 'Price'
    a = ParseData()
    a.ProcessXLSX(n, filename, name, charge, description, category, destination)
