import importlib
import os
import sys
import json

sys.path.append(os.getcwd())

from ParseData import ParseData
import pandas as pd

a = ParseData()
here = os.path.dirname(os.path.abspath(__file__))
folder = os.path.basename(here)

basePath=here.split("Process CDM")[0]
destination=os.path.join(basePath,"CDM")
pathToData=os.path.join(basePath,'Data')
here=os.path.join(os.path.join(pathToData,'Alaska'),folder)
if not os.path.exists(here):
    print("No Data Found For {0}".format(here))
    exit(0)
if not os.path.exists(destination):
    os.mkdir(destination)
print(destination)
state = os.path.join(destination, 'Alaska')  # Storing Statewise
if not os.path.exists(state):
    os.mkdir(state)
destination = os.path.join(state, folder+".csv")
for filename in os.listdir(here):
  if '.csv' in filename:
    filename = os.path.join(here, filename)
    hospitalId='none'
    name=folder
    if os.stat(filename).st_size == 0:
        print('%s is empty, skipping.' % filename)
        continue
    charge=""
    description=""
    if 'drg' in filename.lower():
        n=0
        category = 'DRG'
        description = 'Diagnosis Related Group Description'
        charge='Average Charge'
        a.ProcessCSV(n, filename, hospitalId, charge, description, category, destination)

    else:
       category='Standard'
       n=0
       description ='IVDESC'
       charge = 'Price per Unit'
    if filename.endswith('csv'):
        a = ParseData()
        a.ProcessCSV(n,filename,hospitalId,charge,description,category,destination)
