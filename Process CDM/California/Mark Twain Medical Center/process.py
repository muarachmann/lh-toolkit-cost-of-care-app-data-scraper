import os
import sys
import pandas
sys.path.append(os.getcwd())

from ParseData import ParseData
a = ParseData()
here = os.path.dirname(os.path.abspath(__file__))

folder = os.path.basename(here)
basePath=here.split("Process CDM")[0]
destination=os.path.join(basePath,"CDM")
if not os.path.exists(destination):
    os.mkdir(destination)
pathToData=os.path.join(basePath,'Data')
here=os.path.join(os.path.join(pathToData,'California'),folder)
if not os.path.exists(here):
    print("No Data Found For {0}".format(here))
    exit(0)

state = os.path.join(destination, "California")
if not os.path.exists(state):
    os.mkdir(state)
destination = os.path.join(state, folder+".csv")
if(os.path.exists(destination)):
    os.remove(destination)    #Removing from CDM

                    

for filename in os.listdir(here):
   if 'process' not in filename and 'CDM' in filename:
    name = folder
    filename = os.path.join(here, filename)

    if not os.path.exists(filename):
        print('%s is not found in latest folder.' % filename)
        continue

    if os.stat(filename).st_size == 0:
        print('%s is empty, skipping.' % filename)
        continue
    xls = pandas.ExcelFile(filename)
    for sheet in xls.sheet_names:
        if 'pharmacy cdm' in sheet.lower():
         n=4
         category="Pharmacy"
         df = pandas.read_excel(filename, sheet,skiprows=n)
         for col in df.columns:
            if 'Drug ID' in col:
                description=col
            elif 'Med Charge' in col:
                charge = col
         a = ParseData()
         a.ProcessXLSX(n,  filename, name, charge, description, category, destination, sheet)

        elif 'cdm less' in sheet.lower() or 'chargemaster' in sheet.lower():
            n=4
            category="Standard"
            df = pandas.read_excel(filename, sheet, skiprows=n)
            for col in df.columns:
                if 'BILL DESC' in col:
                    description=col
                elif 'PRICE' in col:
                    charge=col
            a=ParseData()
            a.ProcessXLSX(n, filename, name,  charge, description, category, destination,sheet)


