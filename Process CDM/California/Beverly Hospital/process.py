import os
import sys
import pandas
sys.path.append(os.getcwd())

from ParseData import ParseData
a = ParseData()
here = os.path.dirname(os.path.abspath(__file__))

folder = os.path.basename(here)
basePath=here.split("Process CDM")[0]
destination=os.path.join(basePath,"CDM")
if not os.path.exists(destination):
    os.mkdir(destination)
pathToData=os.path.join(basePath,'Data')
here=os.path.join(os.path.join(pathToData,'California'),folder)
if not os.path.exists(here):
    print("No Data Found For {0}".format(here))
    exit(0)

state = os.path.join(destination, "California")
if not os.path.exists(state):
    os.mkdir(state)
destination = os.path.join(state, folder+".csv")
if(os.path.exists(destination)):
    os.remove(destination)    #Removing from CDM


def ProcessXLSX( n, path, filename, name,  charge, description, category, destination,sheet):
    if filename.endswith('xlsx'):
        print("Parsing %s" % filename)
    content = pandas.read_excel(path, sheet,skiprows=n, usecols=[charge, description], encoding='unicode_escape')

    content.rename(columns={charge: 'Charge', description: 'Description'}, inplace=True)
    content['Category'] = category
    content = content.dropna(how='all')
    # Save data!
    print(content.shape)
    if os.path.exists(destination):
        content.to_csv(destination, mode='a', header=False, encoding='utf-8', index=False)
    else:
        content.to_csv(destination, mode='a', encoding='utf-8', index=False)



for filename in os.listdir(here):
   if 'process' not in filename and 'CDM' in filename:
    name = folder
    filename = os.path.join(here, filename)

    if not os.path.exists(filename):
        print('%s is not found in latest folder.' % filename)
        continue

    if os.stat(filename).st_size == 0:
        print('%s is empty, skipping.' % filename)
        continue
    xls = pandas.ExcelFile(filename)
    for sheet in xls.sheet_names:
        if 'drg' in sheet.lower():
            n=0
            category="DRG"
            charge=""
            description=""
            while(len(charge)==0 or len(description)==0):
                n+=1
                df = pandas.read_excel(filename, sheet,skiprows=n)
                for col in df.columns:
                    if 'DRG Title' in col:
                        charge=col
                    elif 'Avg Billed Charges' in col:
                        description=col
            ProcessXLSX(n, filename, filename, name, charge, description, category,destination,sheet)

        elif 'charge' in sheet.lower():
            n=0
            category="Standard"
            df = pandas.read_excel(filename, sheet, skiprows=n)
            for col in df.columns:
                if 'DESCRIPTION' in col:
                    description=col
                elif 'CHARGE' in col:
                    charge=col
            ProcessXLSX(n, filename, filename, name,  charge, description, category, destination,sheet)


