# LibreHealth's Data Scraper

## About

This Scraper is made with python's most popular Open Source framework **[Scrapy](https://scrapy.org/)** and **[Selenium](https://www.selenium.dev/selenium/docs/api/py/)**.

- **Scrapy** - An Open Source framework for extracting the data from websites 
- **Selenium** - The selenium package is used to automate web browser interaction from Python.
With Selenium webdriver this scraper can also scrap dynamic websites.

This scraper scraps chargemaster of US Hospitals and also process them uniformily to have proper columns.

Available Data of States
- Alaska
- Indiana
- New York

Medicare Data Available for all US States

### Code Organisation

- scrapCDM - This is scrapy project folder , it contains a spider directory which contains scrapy spider to crawl data from web
- Data - This Directory will be generated when user scraps any data using scrapy crawl commands. It will contain scraped data.
- CDM - This will also be generated when user will process data using "python run_processes.py" command

## Getting Started

#### Installing Chromedriver [(Link)](https://chromedriver.storage.googleapis.com/index.html?path=83.0.4103.39/)

##### macOS
```shell
brew install brew-cask
brew cask install chromedriver
```
##### Linux

```shell
apt-get update
apt-get install -y libxss1 libappindicator1 libindicator7
apt-get install unzip
wget -N https://chromedriver.storage.googleapis.com/83.0.4103.39/chromedriver_linux64.zip
unzip chromedriver_linux64.zip
chmod +x chromedriver
mv -f chromedriver /usr/local/share/chromedriver
ln -s /usr/local/share/chromedriver /usr/local/bin/chromedriver
ln -s /usr/local/share/chromedriver /usr/bin/chromedriver
```
##### Windows

1. Download directly from **[(Here)](https://chromedriver.storage.googleapis.com/index.html?path=83.0.4103.39/)**
2. Place "chromedriver.exe" path in System's Environment varible "PATH"

#### Installling ChromeBrowser [(Link)](https://www.google.com/intl/en_in/chrome/)

##### macOS

```shell
brew cask install google-chrome
```    

##### Linux
    
```shell
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
apt install -y ./google-chrome-stable_current_amd64.deb
mv /usr/bin/google-chrome-stable /usr/bin/google-chrome
```    

##### Windows

Download directly from **[(Here)](https://www.google.com/intl/en_in/chrome/)**

##### NOTE:- Make sure chromedriver & Google chrome version is same

#### Installling MDBTOOLS [ Optional If you want to process CDM of Indiana, then install it]

##### macOS

```shell
brew install mdbtools.
```    

##### Linux
    
```shell
apt-get install -y python-dev 
apt-get install -y unixodbc-dev
apt-get install -y mdbtools odbc-mdbtools
```    
##### Windows

1. Open "Process CDM/Indiana/process.py" File
2. Replace usage of "mdb_to_pandas" function to "mdb_to_pandas_windows"
3. Make sure Microsoft Access Drivers are installed

### Remove the following Code from scrapCDM/middlewares.py file

```python
display = Display(visible=0, size=(800, 800))
display.start()
```
This is used in CI Pipeline, because there is no display output for the browser to launch in. 

**Important:- You need to remove these two lines**

#### Installing requirements.txt

```shell
pip install -r requirements.txt
```

#### Scrapping CDM & Processing

Till now there are only three states available, we would highly appreciate any contributions

Run these commands after opening project directory in terminal

##### Alaska

```shell
scrapy crawl example
```
This command will scrap all hospitals urls present in urls.py in scrapCDM directory

##### New York

```shell
scrapy crawl new_york
```

This command will get all data present [(Here)](https://health.data.ny.gov/Health/Hospital-Inpatient-Cost-Transparency-Beginning-200/7dtz-qxmr) . This Data is provided by New York State Health Department.


##### Indiana

```shell
scrapy crawl indiana
```
This command will get all data present [(Here)](https://www.in.gov/isdh/28202.htm) . This Data is provided by Indiana State Health Department.


##### California

```shell
scrapy crawl california
```
This command will get all data present [(Here)](https://oshpd.ca.gov/data-and-reports/cost-transparency/hospital-chargemasters/latest-chargemasters/) . This Data is provided by OSHPD  California State Health Department.


#### Important - This website will not work for other countries so you need to use VPN of US to access this website.


##### Medicare Data

```shell
scrapy crawl medicare
```

This command will get all data present below

**Inpatient Data of 3000 Hospitals**

https://data.cms.gov/Medicare-Inpatient/Inpatient-Prospective-Payment-System-IPPS-Provider/tcsp-6e99

**Outpatient Data of Hospitals**

https://data.cms.gov/Medicare-Outpatient/Provider-Outpatient-Hospital-Charge-Data-by-APC-CY/fmbt-qrrw


### Processing Scraped Data to proper columns

```shell
python run_processes.py
```

This command will run "process.py" script which is different for each hospital & is present in each hospital's directory already.

After this command is executed all parsed data will be saved in "CDM" directory statewise & with proper column names.


#### Naming Convention used

Each process.py script will create a .csv file which has three columns 

- Charge - price of procedure
- Description - description of procedure
- Category - It can be Standard, DRG, Pharmacy

